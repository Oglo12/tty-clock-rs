mod symbols;

use symbols::*;
use colored::Colorize;
use std::env;
use std::process::Command;
use chrono::{Timelike, Local};

const OS: &str = env::consts::OS;
const DEBUG: bool = false;

fn main() {
    let cmd_args: Vec<String> = env::args().collect();

    let mut prev_minute = Local::now().minute();
    let mut set_up = false;

    let char_style_string: String;

    if cmd_args.len() > 1 {
        char_style_string = cmd_args[1].to_lowercase();
    } else {
        char_style_string = String::from("big");
    }

    let char_style = char_style_string.as_str();

    loop {
        let now = Local::now();
        let (is_pm_bool, hour) = now.hour12();
        let minute = now.minute();
        let am_pm = match is_pm_bool {
            true => "pm",
            false => "am",
        };

        if prev_minute != minute || set_up == false {
            prev_minute = minute;
            set_up = true;

            if DEBUG == false {
                clear_terminal();
            }

            println!("{}", get_symbol_string(format!("{}:{} {}", hour, minute, am_pm).as_str(), char_style).cyan());
        }

        if DEBUG == true {
            break;
        }
    }
}

fn clear_terminal() {
    if OS == "windows" {
        Command::new("cmd").args(["/C", "cls"]).status().unwrap();
    } else {
        Command::new("bash").args(["-c", "clear"]).status().unwrap();
    }
}