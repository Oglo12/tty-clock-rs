use std::collections::HashMap;

// OMG! This took so long to do! Enjoy it! :-D

pub fn get_symbol_as_vec(symbol: &str, style: &str) -> Vec<String> {
    let mut big_symbols = HashMap::new();
    let mut block_symbols = HashMap::new();

    big_symbols.insert("info", vec![
        "6".to_string(),
    ]);

    big_symbols.insert(" ", vec![
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
    ]);

    big_symbols.insert("a", vec![
        " █████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "███████║".to_string(),
        "██╔══██║".to_string(),
        "██║  ██║".to_string(),
        "╚═╝  ╚═╝".to_string(),
    ]);

    big_symbols.insert("p", vec![
        "██████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "██████╔╝".to_string(),
        "██╔═══╝ ".to_string(),
        "██║     ".to_string(),
        "╚═╝     ".to_string(),
    ]);

    big_symbols.insert("m", vec![
        "███╗   ███╗".to_string(),
        "████╗ ████║".to_string(),
        "██╔████╔██║".to_string(),
        "██║╚██╔╝██║".to_string(),
        "██║ ╚═╝ ██║".to_string(),
        "╚═╝     ╚═╝".to_string(),
    ]);

    big_symbols.insert(":", vec![
        "   ".to_string(),
        "██╗".to_string(),
        "╚═╝".to_string(),
        "██╗".to_string(),
        "╚═╝".to_string(),
        "   ".to_string(),
    ]);

    big_symbols.insert("1", vec![
        "  ███╗  ".to_string(),
        " ████║  ".to_string(),
        "██╔██║  ".to_string(),
        "╚═╝██║  ".to_string(),
        "███████╗".to_string(),
        "╚══════╝".to_string(),
    ]);

    big_symbols.insert("2", vec![
        "██████╗ ".to_string(),
        "╚════██╗".to_string(),
        "  ███╔═╝".to_string(),
        "██╔══╝  ".to_string(),
        "███████╗".to_string(),
        "╚══════╝".to_string(),
    ]);

    big_symbols.insert("3", vec![
        "██████╗ ".to_string(),
        "╚════██╗".to_string(),
        " █████╔╝".to_string(),
        " ╚═══██╗".to_string(),
        "██████╔╝".to_string(),
        "╚═════╝ ".to_string(),
    ]);

    big_symbols.insert("4", vec![
        "  ██╗██╗".to_string(),
        " ██╔╝██║".to_string(),
        "██╔╝ ██║".to_string(),
        "███████║".to_string(),
        "╚════██║".to_string(),
        "     ╚═╝".to_string(),
    ]);

    big_symbols.insert("5", vec![
        "███████╗".to_string(),
        "██╔════╝".to_string(),
        "██████╗ ".to_string(),
        "╚════██╗".to_string(),
        "██████╔╝".to_string(),
        "╚═════╝ ".to_string(),
    ]);

    big_symbols.insert("6", vec![
        " █████╗ ".to_string(),
        "██╔═══╝ ".to_string(),
        "██████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "╚█████╔╝".to_string(),
        " ╚════╝ ".to_string(),
    ]);

    big_symbols.insert("7", vec![
        "███████╗".to_string(),
        "╚════██║".to_string(),
        "    ██╔╝".to_string(),
        "   ██╔╝ ".to_string(),
        "  ██╔╝  ".to_string(),
        "  ╚═╝   ".to_string(),
    ]);

    big_symbols.insert("8", vec![
        " █████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "╚█████╔╝".to_string(),
        "██╔══██╗".to_string(),
        "╚█████╔╝".to_string(),
        " ╚════╝ ".to_string(),
    ]);

    big_symbols.insert("9", vec![
        " █████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "╚██████║".to_string(),
        " ╚═══██║".to_string(),
        " █████╔╝".to_string(),
        " ╚════╝ ".to_string(),
    ]);

    big_symbols.insert("0", vec![
        " █████╗ ".to_string(),
        "██╔══██╗".to_string(),
        "██║  ██║".to_string(),
        "██║  ██║".to_string(),
        "╚█████╔╝".to_string(),
        " ╚════╝ ".to_string(),
    ]);

    block_symbols.insert("info", vec![
        "5".to_string(),
    ]);

    block_symbols.insert(" ", vec![
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
        "      ".to_string(),
    ]);

    block_symbols.insert("a", vec![
        "   ████   ".to_string(),
        " ██    ██ ".to_string(),
        " ████████ ".to_string(),
        " ██    ██ ".to_string(),
        " ██    ██  ".to_string(),
    ]);

    block_symbols.insert("p", vec![
        " ████████ ".to_string(),
        " ██    ██ ".to_string(),
        " ████████ ".to_string(),
        " ██       ".to_string(),
        " ██       ".to_string(),
    ]);

    block_symbols.insert("m", vec![
        " ██████████ ".to_string(),
        " ██  ██  ██ ".to_string(),
        " ██  ██  ██ ".to_string(),
        " ██  ██  ██ ".to_string(),
        " ██  ██  ██ ".to_string(),
    ]);

    block_symbols.insert(":", vec![
        "    ".to_string(),
        " ██ ".to_string(),
        "    ".to_string(),
        " ██ ".to_string(),
        "    ".to_string(),
    ]);

    block_symbols.insert("1", vec![
        "   ██   ".to_string(),
        " ████   ".to_string(),
        "   ██   ".to_string(),
        "   ██   ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("2", vec![
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        " ██████ ".to_string(),
        " ██     ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("3", vec![
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("4", vec![
        " ██  ██ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        "     ██ ".to_string(),
    ]);

    block_symbols.insert("5", vec![
        " ██████ ".to_string(),
        " ██     ".to_string(),
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("6", vec![
        " ██████ ".to_string(),
        " ██     ".to_string(),
        " ██████ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("7", vec![
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        "     ██ ".to_string(),
        "     ██ ".to_string(),
        "     ██ ".to_string(),
    ]);

    block_symbols.insert("8", vec![
        " ██████ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("9", vec![
        " ██████ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
        "     ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    block_symbols.insert("0", vec![
        " ██████ ".to_string(),
        " ██  ██ ".to_string(),
        " ██  ██ ".to_string(),
        " ██  ██ ".to_string(),
        " ██████ ".to_string(),
    ]);

    let mut symbols = HashMap::new();

    if style == "big" {
        symbols = big_symbols;
    }

    else if style == "block" {
        symbols = block_symbols;
    }

    match symbols.get(symbol) {
        Some(s) => {
            return s.to_vec();
        },
        None => {
            return vec![
                "?".to_string(),
                "?".to_string(),
                "?".to_string(),
                "?".to_string(),
                "?".to_string(),
                "?".to_string(),
            ];
        }
    };
}

pub fn get_symbol_info(style: &str) -> Vec<String> {
    return get_symbol_as_vec("info", style);
}

pub fn get_symbol_string_vec(text: &str, style: &str) -> Vec<String> {
    let mut phrase = String::new();
    let mut phrase_vec: Vec<String> = Vec::new();

    let size: usize = get_symbol_info(style)[0].parse().unwrap();

    for i in 0..size {
        for j in text.chars() {
            phrase.push_str(get_symbol_as_vec(j.to_string().as_str(), style)[i].as_str());
        }

        phrase_vec.push(phrase);

        phrase = String::new();
    }

    return phrase_vec;
}

pub fn get_symbol_string(text: &str, style: &str) -> String {
    let text_vec = get_symbol_string_vec(text, style);
    let mut phrase = String::new();

    for i in 0..text_vec.len() {
        phrase.push_str(text_vec[i].as_str());

        let size: usize = get_symbol_info(style)[0].parse().unwrap();

        if i < size - 1 {
            phrase.push_str("\n");
        }
    }

    return phrase;
}